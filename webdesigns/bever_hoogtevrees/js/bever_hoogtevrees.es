const whBase = require('@webhare-system/compat/base');
require('@webhare-publisher/richcontent/all');
const $ = require('jquery');
require('parsleyjs'); // Parsley needs jQuery
const compatUpload = require('@webhare-system/compat/upload')
const analytics = require('@webhare-socialite/universal-analytics');

// load CSSfunction event(category, action, label, value) {
require('../css/bever_hoogtevrees.scss');

let uploadedFileData = '';
let formAnalyticsSent = false;

// setup DOM-ready
document.addEventListener('DOMContentLoaded', () => {

  // click events
  $('#bh-form-cv').click(uploadFile);

  // Analytics click and change events
  $('#bever-zonder-hoogtevrees .footer__img--desktop').click(function() {
    fireAnalyticsEvent('interaction','click','desktop');
  });

  $('#bever-zonder-hoogtevrees .footer__img--mobile').click(function() {
    fireAnalyticsEvent('interaction','click','mobile');
  });

  // signal Analytics event when editing form
  $('.form__input').change(function() {
    if (!formAnalyticsSent) {
      fireAnalyticsEvent('interaction','formDataEntered');
      formAnalyticsSent = true;
    }
  });

  // form submit handler
  $('#bever-hoogtevrees-form').submit((evt) => {
    evt.preventDefault();

    if ($('#bever-hoogtevrees-form').parsley().isValid()) {
      // gather submitted data
      const formData = { firstName: $('#bh-form-firstname').val(),
                         infix: $('#bh-form-infix').val(),
                         lastName: $('#bh-form-lastname').val(),
                         birthdateDay: $('#bh-form-birthdate_day').val(),
                         birthdateMonth: $('#bh-form-birthdate_month').val(),
                         birthdateYear: $('#bh-form-birthdate_year').val(),
                         email: $('#bh-form-email').val(),
                         motivation: $('#bh-form-motivation').val(),
                         cv: uploadedFileData,
                         isBeverEmployee: $('input:radio[name=bh-form-is-employee]:checked').val(),
                       };

      console.info(formData);

      // FIXME: submit form; callbacks:
      onFormSuccess({ success: true });
      // onFormException({})
    }
  });

  // add days to day pulldown
  for (let i = 1; i <= 31; i++)
    $('#bh-form-birthdate_day').append($('<option value="' + i + '">' + i + '</option>'));

  // add years to birthdate year pulldown
  const yearNow = new Date().getFullYear();
  for (let i = yearNow - 10; i >= yearNow - 100; i--)
    $('#bh-form-birthdate_year').append($('<option value="' + i + '">' + i + '</option>'));

  // setup test/debug data for the form
  if (whBase.debug.debug === true) {
    $('#bh-form-firstname').val('Pietje');
    $('#bh-form-infix').val('van der');
    $('#bh-form-lastname').val('Puk');
    $('#bh-form-birthdate_day').val('14');
    $('#bh-form-birthdate_month').val('2');
    $('#bh-form-birthdate_year').val('1937');
    $('#bh-form-email').val('info@example.com');
    $('#bh-form-motivation').val('Mijn\nmotivatie');
  }

  // setup form validation using Parsley
  const parsleyOptions = { trigger: "change keypress focusout",
                           classHandler: function (el) {
                                           return el.$element.closest('.form-group');
                                         },
                         };

  $('#bever-hoogtevrees-form').parsley(parsleyOptions);
});

// wrapper to make sure we don't send events on dev-servers (the Bever site loads Analytics)
// arguments: category, action, label
function fireAnalyticsEvent(...args) {
  if (whBase.islive) {
    analytics.event(...args);
  } else {
    console.info('analytics event with arguments:', arguments);
  }
}

function uploadFile(evt)
{
  evt.preventDefault();

  fireAnalyticsEvent('interaction','upload-start');
  compatUpload.selectHTML5File({}).addEvents(
  { load: function(event)
          {
            if(!event.files.length)
              return;

            fireAnalyticsEvent('interaction','upload-selected', event.files[0].name);

            var reader = new FileReader;
            reader.onload = async function(file)
            {
              try
              {
                uploadedFileData = reader.result;

                $('#bever-zonder-hoogtevrees .form__filenamecontainer').addClass('visible');
                $('#bever-zonder-hoogtevrees .js-filename').text('FIXME: bestandsnaam');

                //FIXME: size check?
                //FIXME: type check?

                fireAnalyticsEvent('background','gotupload');
              }
              catch(e)
              {
                fireAnalyticsEvent('error','exception',e.message);
                console.error(e);
              }
            };
            reader.readAsDataURL(event.files[0]);
          }
  });
}

function scrollToFormTop() {
  $('html, body').animate({ scrollTop: ($("#bever-zonder-hoogtevrees .formcontainer").offset().top - 45) + 'px' });
}

function onFormSuccess(result = {}) {
  if (result && result.success) {
    $('#bever-zonder-hoogtevrees .form').remove();
    $('#bever-zonder-hoogtevrees .form__result--error').removeClass('visible');
    $('#bever-zonder-hoogtevrees .form__result--success').addClass('visible');

    scrollToFormTop();
  } else {
    onFormException(result);
  }
}

function onFormException(result = {}) {
  $('#bever-zonder-hoogtevrees .form__result--error').addClass('visible');
  console.error(result);
  scrollToFormTop();
}
