# Bever Hoogtevrees

## Installation
```
# Clone the repo and save as 'bever_hoogtevrees'
git clone git@gitlab.com:wh-ict/bever-hoogtevrees.git "$(wh getdatadir)installedmodules/bever_hoogtevrees"

# Make sure WebHare knows about this module
wh softreset
```

## Satisfy the module dependencies
```
whcd bever_hoogtevrees/webdesigns/bever_hoogtevrees/
wh noderun npm install
wh noderun bower install
# if whcd is unavailable, try:
# cd "$(wh getmoduledir bever_hoogtevrees)webdesigns/bever_hoogtevrees/"
```

# Install the site
```
wh sitemgr install 'Bever Hoogtevrees'
```
